package org.tsd.tsdbot.listener;

import org.javacord.api.DiscordApi;
import org.tsd.tsdbot.discord.DiscordMessage;

public abstract class MessageFilter {

    protected final DiscordApi api;

    public MessageFilter(DiscordApi api) {
        this.api = api;
    }

    public abstract boolean isHistorical();
    public abstract void filter(DiscordMessage<?> message) throws MessageFilterException;
}
