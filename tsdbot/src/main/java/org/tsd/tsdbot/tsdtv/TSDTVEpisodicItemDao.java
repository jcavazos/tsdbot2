package org.tsd.tsdbot.tsdtv;

import io.dropwizard.hibernate.AbstractDAO;
import io.dropwizard.hibernate.UnitOfWork;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@SuppressWarnings("unchecked")
public class TSDTVEpisodicItemDao extends AbstractDAO<TSDTVEpisodicItem> {

    private static final Logger log = LoggerFactory.getLogger(TSDTVEpisodicItemDao.class);

    public TSDTVEpisodicItemDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @UnitOfWork
    public List<TSDTVEpisodicItem> queryCurrentEpisode(String searchString) {
        log.info("Querying current episode info for search term: {}", searchString);
        String queryString = "FROM TSDTVEpisodicItem WHERE seriesName LIKE :search";
        Query<TSDTVEpisodicItem> query = query(queryString);
        query.setParameter("search", "%"+searchString+"%");
        return query.getResultList();
    }

    @UnitOfWork
    public List<TSDTVEpisodicItem> queryCurrentEpisode(List<String> searchTerms) {
        log.info("Querying current episode info for search terms: {}", searchTerms);
        StringBuilder queryString = new StringBuilder("FROM TSDTVEpisodicItem");

        for (int i=0 ; i < searchTerms.size() ; i++) {
            if (i == 0) {
                queryString.append(" WHERE ");
            } else {
                queryString.append(" AND ");
            }

            String paramName = ":param"+i;
            queryString.append("(seriesName LIKE ").append(paramName)
                    .append(" OR seasonName LIKE ").append(paramName).append(")");
        }

        Query<TSDTVEpisodicItem> query = query(queryString.toString());

        for (int i=0 ; i < searchTerms.size() ; i++) {
            query.setParameter("param"+i, "%"+searchTerms.get(i)+"%");
        }

        return query.getResultList();
    }

    @UnitOfWork
    public TSDTVEpisodicItem getCurrentEpisode(String seriesName, String seasonName) {
        return currentEpisode(seriesName, seasonName);
    }

    private TSDTVEpisodicItem currentEpisode(String seriesName, String seasonName) {
        log.info("Getting current episode, series={}, season={}", seriesName, seasonName);
        StringBuilder builder = new StringBuilder("FROM TSDTVEpisodicItem WHERE seriesName = :series");
        if (StringUtils.isNotBlank(seasonName)) {
            builder.append(" AND seasonName = :season");
        }

        org.hibernate.query.Query<TSDTVEpisodicItem> query = query(builder.toString());
        query.setParameter("series", seriesName);
        if (StringUtils.isNotBlank(seasonName)) {
            query.setParameter("season", seasonName);
        }

        TSDTVEpisodicItem item = uniqueResult(query);

        if (item == null) {
            log.info("No episode info in database for series={} and season={}, initializing...",
                    seriesName, seasonName);
            item = new TSDTVEpisodicItem();
            item.setSeriesName(seriesName);
            item.setSeasonName(seasonName);
            item.setCurrentEpisode(1);
            persist(item);
        }

        log.info("Returning episodic info: {}", item);
        return item;
    }

    @UnitOfWork
    public void setCurrentEpisode(String seriesName, String seasonName, int episode) {
        log.info("Setting current episode, seriesName={}, seasonName={}, episode={}",
                seriesName, seasonName, episode);
        TSDTVEpisodicItem item = currentEpisode(seriesName, seasonName);
        item.setCurrentEpisode(episode);
        persist(item);
        log.info("Set episodic info: {}", item);
    }
}
