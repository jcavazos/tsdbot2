package org.tsd.tsdbot.discord;

public interface MessageRecipient {
    long getId();
    String getName();
    DiscordMessage<? extends MessageRecipient> sendMessage(String content);
}
