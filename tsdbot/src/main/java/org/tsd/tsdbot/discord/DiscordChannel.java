package org.tsd.tsdbot.discord;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.server.Server;

import java.util.concurrent.TimeUnit;

public class DiscordChannel implements MessageRecipient {

    private final long id;
    private final String name;

    private final Server server;
    private final ServerTextChannel channel;

    public DiscordChannel(ServerTextChannel channel) {
        this.channel = channel;
        this.server = channel.getServer();
        this.id = channel.getId();
        this.name = channel.getName();
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public ServerTextChannel getChannel() {
        return channel;
    }

    public Server getServer() {
        return server;
    }

    @Override
    public DiscordMessage<DiscordUser> sendMessage(String content) {
        try {
            Message message = channel.sendMessage(content).get(10, TimeUnit.SECONDS);
            return new DiscordMessage<>(message);
        } catch (Exception e) {
            throw new RuntimeException("Error sending message to channel: " + this, e);
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("channel", channel)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DiscordChannel that = (DiscordChannel) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .toHashCode();
    }
}
