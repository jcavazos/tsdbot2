package org.tsd.tsdbot.app.module;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import org.apache.commons.lang3.StringUtils;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.server.Server;
import org.tsd.Constants;
import org.tsd.tsdbot.app.DiscordServer;
import org.tsd.tsdbot.app.config.TSDBotConfiguration;
import org.tsd.tsdbot.discord.DiscordUser;

import java.util.Optional;

public class DiscordModule extends AbstractModule {

    private final DiscordApi api;
    private final TSDBotConfiguration configuration;

    public DiscordModule(DiscordApi api, TSDBotConfiguration configuration) {
        this.api = api;
        this.configuration = configuration;
    }

    @Override
    protected void configure() {
        bind(DiscordApi.class).toInstance(api);

        Server tsdirc = api.getServerById(configuration.getServerId())
                .orElseThrow(() -> new RuntimeException("Could not find server with ID "+configuration.getServerId()));

        bind(Server.class)
                .annotatedWith(DiscordServer.class)
                .toInstance(tsdirc);

        Optional<DiscordUser> owner = api.getCachedUsers()
                .stream()
                .filter(user -> StringUtils.equals(configuration.getOwner(), user.getName()))
                .map(DiscordUser::new)
                .findAny();

        if (!owner.isPresent()) {
            throw new RuntimeException("Could not find owner in chat: " + configuration.getOwner());
        }

        bind(DiscordUser.class)
                .annotatedWith(Names.named(Constants.Annotations.OWNER))
                .toInstance(owner.get());

        bind(DiscordUser.class)
                .annotatedWith(Names.named(Constants.Annotations.SELF))
                .toInstance(new DiscordUser(api.getYourself()));
    }
}
